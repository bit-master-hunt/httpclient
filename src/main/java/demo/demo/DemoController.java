package demo.demo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import demo.github.GithubService;
import demo.github.Repo;

@Controller
public class DemoController {
	@Autowired
	private GithubService githubService;
	
	@RequestMapping(value = "/repos", produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
	@ResponseBody
	public DemoResponse get(@RequestParam String user) {
		DemoResponse response = new DemoResponse();
		
		List<Repo> respositories = null;
		String status = "OK";
		try {
			respositories = githubService.getRepos(user);
		} catch(Exception e) {
			status = "ERROR due to " + e.getClass().getName();
		}
		
		response.setRepositories(respositories);		
		response.setStatus(status);
		response.setTimestamp(new Date());
		return response;
	}
}
