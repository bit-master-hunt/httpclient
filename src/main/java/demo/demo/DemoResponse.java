package demo.demo;

import java.util.Date;
import java.util.List;

import demo.github.Repo;

public class DemoResponse {
	private Date timestamp;
	private String status;
	private List<Repo> repositories;
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Repo> getRepositories() {
		return repositories;
	}
	public void setRepositories(List<Repo> repositories) {
		this.repositories = repositories;
	}
}
