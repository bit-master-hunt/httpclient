package demo.github;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class GithubService {
	public static final String GITHUB_HOST = "api.github.com/users";
	public static final String REPO_ENDPOINT = "/repos";
	
	public List<Repo> getRepos(String user) throws URISyntaxException, ClientProtocolException, IOException {
		URI uri = new URIBuilder()
		.setScheme("https")
		.setHost(GITHUB_HOST)
		.setPath("/" + user + REPO_ENDPOINT)	
		.build();
			
		HttpGet httpget = new HttpGet(uri);		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
		        .setSocketTimeout(1000)
		        .setConnectTimeout(1000)
		        .build();
		
		httpget.setConfig(requestConfig);		
		CloseableHttpResponse response1 = httpclient.execute(httpget);		
		
		HttpEntity result = response1.getEntity();
		InputStream stream = result.getContent();			
		ObjectMapper mapper = new ObjectMapper();
		List<Repo> repos = mapper.readValue(stream, new TypeReference<List<Repo>>(){});		
		stream.close();
		httpclient.close();
		
		Collections.sort(repos, new Comparator<Repo>() {
			@Override
			public int compare(Repo r1, Repo r2) {
				return r2.getWatchersCount() - r1.getWatchersCount();
			}
			
		});
		return repos;
	}
}
