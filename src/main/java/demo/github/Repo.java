package demo.github;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repo {
	private Long id;
	private String name;
	private Owner owner;
	@JsonProperty("private")
	private Boolean isPrivate;
	@JsonProperty("watchers_count")
	private Integer watchersCount;
	
	public Integer getWatchersCount() {
		return this.watchersCount;
	}	
	public void setWatchersCount(Integer wc) {
		this.watchersCount = wc;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	public Boolean getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
}
